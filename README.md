Hi 👋  My name is Ben Grandin
===========================================================================================================================


Fullstack TS developer | Freelance
----------------------------------

* 🌍 I'm based in France
* ✉️ You can contact me at [bengrandin@hotmail.com](mailto:bengrandin@hotmail.com)
* 🧠 I'm learning Nuxt
* ⚡ I love outdoor sport like Parkour or Climbing


### More about me

Since I started in web development in 2018, I have navigated through a variety of environments, from innovative startups to large corporations and ESNs.

I decided to go freelance to share my experience and choose my assignments. This decision allows me to prioritize projects that are close to my heart, especially those that aim to have a positive impact on society. My dream mission would be in EdTech.

Experienced in TypeScript, Vue.js, React, and Node.js, I offer tailored and efficient solutions that meet the diverse needs of projects.

Convinced that the involvement of developers in strategic decisions leads to more efficient products aligned with user expectations, I like to actively contribute beyond coding.

I regularly participate in events and meetups on topics such as Agility, TypeScript, and JavaScript, which allows me to stay connected with the latest trends and share knowledge with peers.

Training for future web developers:
If you aspire to become a web developer, this list of training will effectively prepare you for the necessary front-end and back-end skills in this field:
https://docs.google.com/document/d/1TkFkYmOBmQG3xHo2MN93XYlmGfi9txX7f_6VL_AOUXI

## Skills

### Front
<p align="left">
<a href="https://www.typescriptlang.org/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/typescript-colored.svg" width="36" height="36" alt="TypeScript" />
</a>
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/javascript-colored.svg" width="36" height="36" alt="JavaScript" />
</a>
</p>
<p>
<a href="https://reactjs.org/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/react-colored.svg" width="36" height="36" alt="React" />
</a>
<a href="https://nextjs.org/docs" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/nextjs-colored-dark.svg" width="36" height="36" alt="NextJs" />
</a>
<a href="https://vuejs.org/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/vuejs-colored.svg" width="36" height="36" alt="Vue" />
</a>
<a href="https://nuxtjs.org/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/nuxtjs-colored.svg" width="36" height="36" alt="Nuxtjs" />
</a>
</p>
<p>
<a href="https://sass-lang.com/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/sass-colored.svg" width="36" height="36" alt="Sass" />
</a>
<a href="https://tailwindcss.com/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/tailwindcss-colored.svg" width="36" height="36" alt="TailwindCSS" />
</a>
<a href="https://mui.com/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/materialui-colored.svg" width="36" height="36" alt="Material UI" />
</a>
</p>

### Back
<p>
<a href="https://nodejs.org/en/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/nodejs-colored.svg" width="36" height="36" alt="NodeJS" />
</a>
<a href="https://expressjs.com/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/express-colored-dark.svg" width="36" height="36" alt="Express" />
</a>
<a href="https://docs.nestjs.com/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/nestjs-colored.svg" width="36" height="36" alt="NestJS" />
</a>
</p>

### Others
<a href="https://graphql.org/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/graphql-colored.svg" width="36" height="36" alt="GraphQL" />
</a>
<a href="https://www.docker.com/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/docker-colored.svg" width="36" height="36" alt="Docker" />
</a>

## Socials
<p>
<a href="https://www.github.com/bengrandin" target="_blank" rel="noreferrer"> <picture> <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/github-dark.svg" /> <source media="(prefers-color-scheme: light)" srcset="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/github.svg" /> <img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/github.svg" width="32" height="32" /> </picture> </a>
<a href="https://www.stackoverflow.com/users/12373992/ben-grandin" target="_blank" rel="noreferrer"> <picture> <source media="(prefers-color-scheme: dark)" srcset="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/stackoverflow.svg" /> <source media="(prefers-color-scheme: light)" srcset="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/stackoverflow.svg" /> <img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/socials/stackoverflow.svg" width="32" height="32" /> </picture> </a>
</p>

